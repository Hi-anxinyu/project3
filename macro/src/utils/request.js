const Fly = require("flyio/dist/npm/wx");
const fly = new Fly;
fly.config = {
    baseURL: 'https://capi.mwee.cn',
    timeout: 5000
}
const body = {
    "cityId": 19,
    "longitude": 116.29844665527344,
    "latitude": 39.95933151245117,
    "mwAuthToken": "uPCQvKl9RhX5r9SGP2noD0Ei0D3egOAN8IrKQ9lIY-BS0nZ8NKqZIMOel3oNkoUYqtfpxVT9s6xsYveX-CrA3ahoPYXmpdLdIF3rNSY-BJe8dSyjs7Y-B8dBOeb0Y-BF7iOpdysSY-BHg10FT1hXfs2WAlXu7D8j0009rAAuYJjBYuC96KrAbYzJuBedLQMNPmBK6sz4muafrm1u2ZIbmPpChaX-Ce3QuHcg-Z-Z",
    "mAuthToken": "uPCQvKl9RhX5r9SGP2noD0Ei0D3egOAN8IrKQ9lIY-BS0nZ8NKqZIMOel3oNkoUYqtfpxVT9s6xsYveX-CrA3ahoPYXmpdLdIF3rNSY-BJe8dSyjs7Y-B8dBOeb0Y-BF7iOpdysSY-BHg10FT1hXfs2WAlXu7D8j0009rAAuYJjBYuC96KrAbYzJuBedLQMNPmBK6sz4muafrm1u2ZIbmPpChaX-Ce3QuHcg-Z-Z",
    "fromw": 1401,
    'mwtoken': 'uPCQvKl9RhX5r9SGP2noD0Ei0D3egOAN8IrKQ9lIY-BS0nZ8NKqZIMOel3oNkoUYqtroKHKe0Y-BmgBt6S4mPj0mdyhA4Y-BEELmehQZ2vwpvliJY7Y-B8dBOeb0Y-BF7iOpdysSY-BHg10FT1hXfs2WAlXu7D8j0xIRvTM2kppxwjqJBruqrVxG6VbfJHmhW6PRcpft5dourm1u2ZIbmPpChaX-Ce3QuHcg-Z-Z',
    // 'isnew': 1,
    // 'isposter': 0,
    // 'shopid': 0,
    'from':	'wxapp',
    // 'zfid':	0,
    'op': 'info'
}
fly.interceptors.request.use((request) => {
    console.log('request...', request);
    if (request.body) {
        request.body = { ...request.body, ...body }
    } else {
        request.body = body;
    }
    //给所有请求添加自定义header
    request.headers["X-Tag"] = "flyio";
    if (request.url.indexOf('php') > 0) {
        request.headers['Content-Type'] = 'application/x-www-form-urlencoded';
    }
    //打印出请求体
    // console.log(request.body)
    //终止请求
    //ar err=new Error("xxx")
    //err.request=request
    //return Promise.reject(new Error(""))

    //可以显式返回request, 也可以不返回，没有返回值时拦截器中默认返回request
    return request;
})

//添加响应拦截器，响应拦截器会在then/catch处理之前执行
fly.interceptors.response.use(
    (response) => {
        //只将请求结果的data字段返回
        return response.data
    },
    (err) => {
        //发生网络错误后会走到这里
        //return Promise.resolve("ssss")
    }
)
// fly.interceptors.request.use((request) => {
//     //给所有请求添加自定义header
//     if (request.body) {
//         request.body = {...request.body, ...body}
//     } else {
//         request.body = body;
//     }
//     request.headers["X-Tag"] = "flyio";
//     request.headers['Content-Type'] = 'application/x-www-form-urlencoded'
//     //打印出请求体
//     //终止请求
//     //var err=new Error("xxx")
//     //err.request=request
//     //return Promise.reject(new Error(""))

//     //可以显式返回request, 也可以不返回，没有返回值时拦截器中默认返回request
//     return request;
// })

// //添加响应拦截器，响应拦截器会在then/catch处理之前执行
// fly.interceptors.response.use((response) => {
//         response.headers = {...response.headers, 'Content-type': 'text/html'}
//         //只将请求结果的data字段返回
//         return response.data
//     },
//     (err) => {
//         //发生网络错误后会走到这里
//         //return Promise.resolve("ssss")
//     }
// )

export default fly;
