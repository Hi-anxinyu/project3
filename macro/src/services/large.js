import request from '@/utils/request';

export const geiGoodList = () => {
  return request.post('/basic/pageConfig/tabs', {
    "cityId": 19,
    "tabId": 328,
    "showPage": 1,
    "platform": 0,
    "showSort": 1,
    "showSortMode": 1,
    "pageNo": 1,
    "pageSize": 10,
  })
}

export const getTabs = () => {
  return request.post('/basic/pageConfig/tabs', {
    "cityId": 19,
    "showPage": 1,
    "platform": 0,
  })
}

export const getApp = () => {
  return request.post('/msh/app/index.php?i=5&t=0&v=1.0.1&from=wxapp&c=entry&a=wxapp&do=index&&m=zofui_sales&sign=7539172ac77676d57b29bc46f329c722')
}

export const defaultLocation = () => {
  return request.post('/api-comm/api/comm.location2city', {
  })
};

export const cityConfig = () => { // 获取城市
  return request.post('/c_msh/mLife/common/cityConfig', {
  })
}

export const getDistrictList = () => { // 获取城市相应地区
  return request.post('/c_msh/mLife/common/getDistrictList', {
    "cityId": 19, // 城市id
  })
}

export const getSwiperImg = () => {
  return request.post('/msh/app/index.php?i=5&t=0&v=1.0.1&from=wxapp&c=entry&a=wxapp&do=index&&m=zofui_sales&sign=911479c2b4e184d54decea1d392eb3cb')
}
