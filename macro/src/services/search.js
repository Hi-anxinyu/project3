import request from '../utils/request';

//搜索页初始数据接口
export const getRankList = (payload) => {
    return request.post('/basic/search/home',payload);
}

//搜索页搜索接口
export const getSearchList = (payload) =>{
    return request.post('/basic/search/suggest',{kw:payload})
}