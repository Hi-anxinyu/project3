import request from '../utils/request';

//我的页面
export const getMyData = ()=> {
    return request.post('/c_msh/mLife/users/userGet');
}
export const getMyImg=()=>{
    return request.get('https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83erMg0xmTHdoQjoZmrv9rxPNWr9ztsPs3Fh3iatgAxgSo6zic8SmnwkD2PqVqib2zxbRNcvM0OVbEe8FA/132',{responseType: 'arraybuffer'})
}