import Vue from 'vue'
import App from './App'
// 引入vuex
import store from '@/store'
import uView from 'uview-ui';
import './utils/font/iconfont.css'
// 引入过滤器
import * as filters from '@/filters';
for (let filter in filters) {
  Vue.filter(filter, filters[filter]);
}

Vue.config.productionTip = false;
Vue.prototype.$store = store;
Vue.use(uView);
App.mpType = 'app'

const app = new Vue({
  // store,
  ...App,
})
app.$mount()
