import Vuex from 'vuex';
import Vue from 'vue';
import createLogger from 'vuex/dist/logger';
// 引入子模块
import home from './modules/home';
import search from './modules/search';
import my from './modules/my'
import large from './modules/large'
Vue.use(Vuex);


export default new Vuex.Store({
    modules: {
        home,
        search,
        my,
        large
    },
    plugins: [createLogger()]
});
