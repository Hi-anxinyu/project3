import {getRankList,getSearchList} from "@/services";

const state = {
    rankList:[],
    baseArea:[],
    shops:[],
    searchList:[],
}

const getters = {
    
}

const mutations = {
    update(state,payload){
        for(let key in payload){
            state[key] = payload[key];
        }
    }
}

const actions = {
    async getRankList({commit},payload){
        let res = await getRankList();
        if(res.errNo===0){
            commit('update',{
                rankList:res.data.rank,
                baseArea:res.data.baseArea,
                shops:res.data.shops
            })
        }
    },
    async getSearchList({commit},payload){
        let res = await getSearchList(payload);
        if(res.errNo===0){
            commit('update',{
                searchList:res.data.list,
            })
        }
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}