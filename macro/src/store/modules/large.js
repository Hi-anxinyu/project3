import { getTabs, getSwiperImg } from '@/services';
const state = {
  larlist: {},
  tagsList: []
}

const getters = {};

const mutations = {
  updateState(state, payload) {
    for(let key in payload) {
      state[key] = payload[key];
    }
  }
}

const actions = {
  async getTabs({commit}, payload) {
    let result = await getTabs();
    commit('updateState', {
      tagsList: result.data,
    })
  },
  async getSwiperImg({commit}, payload) {
    let result = await getSwiperImg();
    commit('updateState', {
      larlist: result.data
    })
  }
};

export default{
  namespaced: true,
  getters,
  mutations,
  actions,
  state
}