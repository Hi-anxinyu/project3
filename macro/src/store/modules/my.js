import {getMyData,getMyImg} from '@/services'

const state={
    MyImageSrc:'',
    uid:'',
    nickName:''
}
const getters = {

}
const mutations={
    update(state,payload){
        for(let key in payload) {
            state[key] = payload[key]
        }
    }
}

const actions ={
    async getMyData({commit},payload){
        let result = await getMyData({"checkMobile": 1,})
        console.log('result....', result);
        if(result.errNo===0){
            commit('update',{uid:result.data.uid,nickName:result.data.nickName})
        }
    },
    async getMyImg({commit},payload){
        let result = await getMyImg()
        commit('update',{MyImageSrc:result})
    }
}


export default {
    namespaced:true,
    state,
    getters,
    mutations,
    actions,
}